/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page77;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.annotation.WebInitParam;

/**
 *
 * @author bumerrrang
 */
@WebServlet(name = "DatabaseAccess", urlPatterns = {"/DatabaseAccess"}, initParams = {
    @WebInitParam(name = "driverClassName", value = "org.postgresql.Driver"),
    @WebInitParam(name = "url", value = "jdbc:postgresql://localhost:5432/test"),
    @WebInitParam(name = "username", value = "postgres"),
    @WebInitParam(name = "password", value = "")})
public class DatabaseAccess extends HttpServlet {
    // JDBC driver name and database URL
    Connection conn;
    
    @Override
    public void init() {
        ServletConfig config = getServletConfig();
        String JDBC_DRIVER = config.getInitParameter("driverClassName");
        String DB_URL = config.getInitParameter("url");
        String USER = config.getInitParameter("username");
        String PASS = config.getInitParameter("password");
        try {
            // Register JDBC driver
            Class.forName(JDBC_DRIVER);
            // Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DatabaseAccess.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DatabaseAccess</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DatabaseAccess at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        // Set response content type
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Database Result";
        Statement stmt;
        stmt = null;
        String docType ="<!doctype html public \"-//w3c//dtd html 4.0 " +
                        "transitional//en\">\n";
        out.println(docType + "<html>\n" +
                    "<head><title>" + title + "</title></head>\n" +
                    "<body bgcolor=\"#f0f0f0\">\n" +
                    "<h1 align=\"center\">" + title + "</h1>\n");
        try{
            // Execute SQL query
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT id, first, last, age FROM employees";
            // Extract data from result set
            try (ResultSet rs = stmt.executeQuery(sql)) {
                // Extract data from result set
                while(rs.next()){
                    //Retrieve by column name
                    int id = rs.getInt("id");
                    int age = rs.getInt("age");
                    String first = rs.getString("first");
                    String last = rs.getString("last");
                    //Display values
                    out.println("ID: " + id);
                    out.println(", Age: " + age);
                    out.println(", First: " + first);
                    out.println(", Last: " + last + "<br>");
                }   out.println("</body></html>");
                // Clean-up environment
            }
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }//end finally try
    } //end try

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
