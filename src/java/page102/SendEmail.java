/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page102;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author bumerrrang
 */
@WebServlet(name = "SendEmail", urlPatterns = {"/SendEmail"})
public class SendEmail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SendEmail</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SendEmail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {
//        try {
            //        processRequest(request, response);
            // Recipient's email ID needs to be mentioned.
            //        String to = "abcd@gmail.com";
            String to = "andrei.lunin.0@gmail.com";
            // Sender's email ID needs to be mentioned
            String from = "andrei.lunin.0@gmail.com";
            // Assuming you are sending email from localhost
//            String host = "localhost";
            String host = "smtp.googlemail.com";
            // Get system properties
            Properties properties = System.getProperties();
            
            // FROM http://stackoverflow.com/questions/14089533/error-in-sending-mail-connectexception
            properties.put("mail.smtp.starttls.enable", "true"); 
            properties.put("mail.smtp.host", host);
            properties.put("mail.smtp.user", from); // User name
            properties.put("mail.smtp.password", "3830206Izplaneta350"); // password
            properties.put("mail.smtp.port", "587");
            properties.put("mail.smtp.auth", "true");
            
//            properties.put("mail.transport.protocol", "smtp");
//            properties.put("mail.smtp.host", "localhost");
//            properties.put("mail.smtp.auth", "false");// set to false for no username
//            properties.put("mail.debug", "false");
//            properties.put("mail.smtp.port", "25");

            // Setup mail server
//            properties.setProperty("mail.smtp.host", host);
            //        properties.setProperty("smtp.googlemail.com", host);
            // Get the default Session object.
            
            /* FROM http://stackoverflow.com/questions/6610572/javax-mail-authenticationfailedexception-failed-to-connect-no-password-specifi
            Session session = Session.getDefaultInstance(props,
    new Authenticator() {
        protected PasswordAuthentication  getPasswordAuthentication() {
        return new PasswordAuthentication(
                    "myemail@gmail.com", "password");
                }
    });
            */
//            Session session = Session.getDefaultInstance(properties);
            //        Session session = Session.getInstance(properties);
            Session session = Session.getDefaultInstance(properties, new Authenticator() {
                                    @Override
                                    protected PasswordAuthentication  getPasswordAuthentication() {
                                        return new PasswordAuthentication(to, "3830206Izplaneta350");
                                    }
            });
            // Set response content type
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();

            // with the Transport, check this out:
            // http://stackoverflow.com/questions/8907229/transport-sendmessage-not-working-in-the-below-code-netbeans-gets-stuck-at-t
//            Transport transport = session.getTransport("smtp");
//            transport.connect();

            try {
                // Create a default MimeMessage object.
                MimeMessage message = new MimeMessage(session);
                // Set From: header field of the header.
                message.setFrom(new InternetAddress(from));
                // Set To: header field of the header.
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                // Set Subject: header field
                message.setSubject("This is the Subject Line!");
                // Now set the actual message
                message.setText("This is actual message");

                // Send message
                Transport.send(message);
//            transport.sendMessage(message, message.getAllRecipients());
                String title = "Send Email";
                String res = "Sent message successfully....";
                String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " +
                    "transitional//en\">\n";
                out.println(docType + "<html>\n" +
                    "<head><title>" + title + "</title></head>\n" +
                    "<body bgcolor=\"#f0f0f0\">\n" +
                    "<h1 align=\"center\">" + title + "</h1>\n" +
                    "<p align=\"center\">" + res + "</p>\n" +
                    "</body></html>");
            }catch (MessagingException mex) {
                mex.printStackTrace();
            } /*finally {
                if(transport!=null) {
                    try {
                        transport.close();
                    } catch (MessagingException ex) {
                        Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        }catch (NoSuchProviderException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(SendEmail.class.getName()).log(Level.SEVERE, null, ex);
        }
        */    
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
