/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package page94;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;

/**
 *
 * @author bumerrrang
 */
@WebFilter(filterName = "SiteHitCounter", urlPatterns = {"/*"})
public class SiteHitCounter implements Filter {
    private int hitCount;
    
    /**
     *
     * @param config
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig config) throws ServletException{
        // Reset hit counter.
        hitCount = 0;
    }
    
    /**
     *
     */
    @Override
    public void destroy() {
        // This is optional step but if you like you
        // can write hitCount value in your database.
    }
    
    /**
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws java.io.IOException, ServletException {
        // increase counter by one
        hitCount++;
        // Print the counter.
        System.out.println("The SiteHitCounter output: Site visits count :"+ hitCount );
        // Pass request back down the filter chain
        chain.doFilter(request,response);
    }
}
